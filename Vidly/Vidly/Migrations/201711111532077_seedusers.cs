namespace Vidly.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedusers : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'ac5876ed-5d4e-41f4-a6b1-9db9ad7ab829', N'guest@vidly.com', 0, N'AOqxmjzqF0EBaDgM5mtcVj0TR3bTZz6rdWjCxSqRDoZ01Bs17YbrXUahAWpbKym4Nw==', N'061340a8-0608-479e-9d71-73b55f2092d8', NULL, 0, 0, NULL, 1, 0, N'guest@vidly.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e00626eb-b54f-4f62-bb9c-aec14d190b36', N'admin@vidly.com', 0, N'AGULPT8RKt3GL+Ld3NZTQ6W79R2O8SVj44DKzLqGKw9L/BJK9rtgmIKkm0jbDdrH2A==', N'cb846b9f-59a8-4f91-8c23-1956d5b586d5', NULL, 0, 0, NULL, 1, 0, N'admin@vidly.com')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'f5fa26d5-01cd-40e3-81b7-10e18bff5a22', N'CanManageMovies')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e00626eb-b54f-4f62-bb9c-aec14d190b36', N'f5fa26d5-01cd-40e3-81b7-10e18bff5a22')

");
        }
        
        public override void Down()
        {
        }
    }
}
