﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace Vidly.Models
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Display(Name = "Release Date")]
        [Required]
        public DateTime ReleaseDate { get; set; }
        public DateTime DateAdded { get; set; }
        [Display(Name = "Number In Stock")]
        [Required]
        [Range(1, 20)]
        public int NumberInStock { get; set; }
        public byte NumberAvailable { get; set; }
        public Genre Genre { get; set; } // links genre to movie
        [Required] //Req needs to be here rather than on genre , due to entity validation error
        //we put this on genreId as it can relate to genre class
        public int GenreId { get; set; } // by convention, entity treats this as a foregin key
        
    }

    
}