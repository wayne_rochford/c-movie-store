﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Vidly.Models
{
    public class Rental
    {
        public int Id { get; set; }
        [Required]
        public Customer Customer { get; set; } // links rental to customer
        [Required]
        public Movie Movie { get; set; } // links rental to movie

        public DateTime DateRented { get; set; }

        public DateTime? DateRentured { get; set; }
    }
}