﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;
using Vidly.Models;
using Vidly.ViewModels;

namespace Vidly.Controllers
{
    public class CustomersController : Controller
    {
        private ApplicationDbContext _context;

        public CustomersController()
        {
             _context = new ApplicationDbContext();   
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        public ViewResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            var MembershipTypes = _context.MembershipTypes.ToList();
            var VM = new CustomerFormViewModel
            {
                Customer = new Customer(),
                MembershipTypes = MembershipTypes
            };

            return View("CustomerForm", VM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save(Customer customer)
        {
            //if model state is not valid return same view
           //customer form (this is validation)

            if(!ModelState.IsValid)
            {
                var VM = new CustomerFormViewModel
                {
                    Customer = customer,
                    MembershipTypes = _context.MembershipTypes.ToList()
                };

                return View("CustomerForm", VM);
            }

            if (customer.Id == 0)
            {
                _context.Customers.Add(customer);
            }
            else
            {
                var CustomerInDB = _context.Customers.Single(c => c.Id == customer.Id);
                //Could use Automapper for all this eg. Mapper.Map(customer, customerinDB)
                CustomerInDB.Name = customer.Name;
                CustomerInDB.DateofBirth = customer.DateofBirth;
                CustomerInDB.MembershipTypeId = customer.MembershipTypeId;
                CustomerInDB.IsSubscribedToNewsletter = customer.IsSubscribedToNewsletter;
                //TryUpdateModel(CustomerInDB); Possibly sec holes
                //UpdateCustomerDto dta trans obj = small represnt of customer with only props that can be updated 
            }
            _context.SaveChanges();
            return RedirectToAction("Index", "Customers");
        }

        public ActionResult Edit(int id)
        {
            var customer = _context.Customers.SingleOrDefault(c => c.Id == id);
            if(customer == null)
            {
                return HttpNotFound();
            }

            var VM = new CustomerFormViewModel
            {
                Customer = customer,
                MembershipTypes = _context.MembershipTypes.ToList()
            };

            return View("CustomerForm", VM);
        }

        public ActionResult Details(int id)
        {
            var customer = _context.Customers.Include(c => c.MembershipType).SingleOrDefault(c => c.Id == id);

            if (customer == null)
                return HttpNotFound();

            return View(customer);
        }

        //private IEnumerable<Customer> GetCustomers()
        //{
        //    return new List<Customer>
        //    {
        //        new Customer { Id = 1, Name = "Prince Devitt" },
        //        new Customer { Id = 2, Name = "Adam Cole" },
        //        new Customer { Id = 3, Name = "Matt Jackson" },
        //        new Customer { Id = 4, Name = "Nick Jackson" },
        //        new Customer { Id = 5, Name = "AJ Styles" },
        //        new Customer { Id = 6, Name = "Kenny Omega" }
        //    };
        //}
    }
}