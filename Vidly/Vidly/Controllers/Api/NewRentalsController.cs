﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Vidly.Dto;
using Vidly.Models;
using System.Data.Entity;

namespace Vidly.Controllers.Api
{
    public class NewRentalsController : ApiController
    {

        private ApplicationDbContext _context;

        public NewRentalsController()
        {
            _context = new ApplicationDbContext();
        }

      
        // POST /api/rentals
        [HttpPost]
        public IHttpActionResult NewRental(NewRentalDto newRental)
        {
            //get customer from picklist
            var customer = _context.Customers.Single(
                c => c.Id == newRental.CustomerId);
            //select * from movies where id in (1 , 2 , 3)
            var movies = _context.Movies.Where(
                m => newRental.MovieIds.Contains(m.Id)).ToList();

            foreach (var movie in movies)
            {
                if(movie.NumberAvailable == 0)
                {
                    return BadRequest("Movie is not available");
                }

                //decrease the avaialbilty by 1 for preformance
                movie.NumberAvailable--;
                var rental = new Rental
                {
                    Customer = customer,
                    Movie = movie,
                    DateRented = DateTime.Now
                };

                _context.Rentals.Add(rental);
            }


                _context.SaveChanges();
            


            return Ok();
        }

    }
        
}
